using System.Collections.Generic;

public static class Algo
{
    public static int CalculateIncrease(List<int> list)
    {
        var result = 0;
        var k = 0;

        while (k < list.Count)
        {
            if (k != 0 && list[k - 1] < list[k])
                result++;
            ++k;
        }

        return result;
    }
}