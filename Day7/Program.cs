﻿using System.Linq;
using System.IO;

public static class Program
{
    public static string AvaiableCharacters = "0123456789,";

    public static void Main(string[] args)
    {
        if (args.Count() != 1)
            throw new FileNotFoundException("input file is missing");
        
        var inputText = File.ReadAllText(args[0]);

        if (!SoftLexer(inputText))
            throw new InvalidDataException();
        var modele = new Modele();

        modele.Crabs = inputText
                    .Split(',')
                    .Select(int.Parse)
                    .ToList();

        Console.WriteLine($"");
        Console.WriteLine($"xxxxxxxx : Pos - Fuel");
        Console.WriteLine($"Actual   :  {modele.CalculateBestPosition()}  -  {modele.TotalFuel}");
        Console.WriteLine($"Expected :  2  -  37");
        Console.WriteLine($"");
    }

    static bool SoftLexer(string input)
    {
        if (string.IsNullOrWhiteSpace(input))
            return false;

        foreach (var c in input)
        {
            if (!AvaiableCharacters.Contains(c))
                return false;
        }
        return true;
    }

}