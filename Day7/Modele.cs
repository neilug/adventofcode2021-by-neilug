using System;
using System.Collections.Generic;

public class Modele
{
    public List<int> Crabs { get; set; }
    public int BestPosition { get; set; }
    public int TotalFuel { get; set; }

    public Modele()
    {
        Crabs = new List<int>();
        BestPosition = 0;
        TotalFuel = 0;
    }

    public int CalculateBestPosition()
    {
        var autoFuel = Crabs.Min();

        while (autoFuel <= Crabs.Max())
        {
            var newTotalFuel =  GetTotalFuel(GetFuelConsumption(autoFuel, Crabs));

            if (TotalFuel == 0 || TotalFuel > newTotalFuel)
            {
                TotalFuel = newTotalFuel;
                BestPosition = autoFuel;
            }
            ++autoFuel;
        }

        return BestPosition;
    }

    private int GetTotalFuel(List<int> fuelConsumptionPerCrabs)
    {
        return fuelConsumptionPerCrabs.Sum();
    }

    private List<int> GetFuelConsumption(int horizontalMovement, List<int> crabs)
    {
        var result = new List<int>();

        foreach (var crab in crabs)
        {
            result.Add(Math.Abs(crab - horizontalMovement));
        }

        return result;
    }
}