using System;
using System.Collections.Generic;
using System.Linq;
namespace Day6.Modeles;

public class LanternFish
{
    public static int NumberOfDayUntilBirth = 7;
    public static int DefaultInternalTimer = 8;
    public readonly int InternalTimer;

    public LanternFish()
    {
        InternalTimer = DefaultInternalTimer;
    }

    public LanternFish(int initialTimer)
    {
        InternalTimer = initialTimer;
    }

    public int NumberOfSpawn(int numberOfDaysLeft)
    {
        if (numberOfDaysLeft - InternalTimer <= 1)
            return 1;
        
        var result = 0;
        var newSpawns = CreateNSpawns(GetNumberOfSpawn(numberOfDaysLeft));

        foreach (var newSpawn in newSpawns)
        {
            result += newSpawn.NumberOfSpawn(numberOfDaysLeft - InternalTimer);
        }

        return result;
    }

    public int GetNumberOfSpawn(int numberOfDaysLeft)
    {
        return (int)Math.Ceiling(Decimal.Divide(numberOfDaysLeft - InternalTimer - 1, NumberOfDayUntilBirth));
    }

    public List<LanternFish> CreateNSpawns(int numberOfSpawnToCreate)
    {
        if (numberOfSpawnToCreate < 0)
            return null;
        return Enumerable.Range(0, numberOfSpawnToCreate).Select(x => new LanternFish()).ToList();
    }
}