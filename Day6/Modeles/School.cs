using System;
using System.Linq;
using System.Collections.Generic;
using Day6.Modeles;

public class School
{
    private string AllowedStrings = "0123456789,";
    public List<LanternFish> EpicSchool { get; set;}
    public readonly int NumberOfDaysOfLifeSpan;

    public School(string initialState, int numberOfDaysOfLifeSpan)
    {
        if (!SoftLexer(initialState))
            throw new Exception("Invalid Input");

        EpicSchool = initialState
            .Split(',')
            .Select(int.Parse)
            .Select(x => new LanternFish(x))
            .ToList();

        NumberOfDaysOfLifeSpan = numberOfDaysOfLifeSpan;
    }

    private bool SoftLexer(string input)
    {
        foreach (var c in input)
        {
            if (!AllowedStrings.Contains(c))
                return false;
        }

        return true;
    }

    public int NumberOfSpawns()
    {
        var result = 0;

        foreach (var lanternFish in EpicSchool)
        {
            result += lanternFish.NumberOfSpawn(NumberOfDaysOfLifeSpan);
        }

        return result;
    }
}