using System;
using System.Collections.Generic;

public class Algo
{
    public int Result { get; set; }

    public Algo(List<List<int>> input)
    {
        int y = 0;
        int x = 0;

        while (y < input.Count)
        {
            x = 0;
            
            while (x < input[y].Count)
            {
                if (VerifyDown(input, x, y) &&
                VerifyUp(input, x, y) &&
                VerifyLeft(input, x, y) &&
                VerifyRight(input, x, y))
                    Result += input[y][x] + 1;
                ++x;
            }
            ++y;
        }
    }

    private bool VerifyLeft(List<List<int>> input, int x, int y)
    {
        if (x == 0)
            return true;
        if (input[y][x - 1] > input[y][x])
            return true;
        return false;
    }
    private bool VerifyRight(List<List<int>> input, int x, int y)
    {
        if (x + 1 >= input[y].Count)
            return true;
        if (input[y][x + 1] > input[y][x])
            return true;
        return false;
    }
    private bool VerifyUp(List<List<int>> input, int x, int y)
    {
        if (y == 0)
            return true;
        if (input[y - 1][x] > input[y][x])
            return true;
        return false;
    }
    private bool VerifyDown(List<List<int>> input, int x, int y)
    {
        if (y + 1 >= input.Count)
            return true;
        if (input[y + 1][x] > input[y][x])
            return true;
        return false;
    }

}