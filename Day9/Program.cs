﻿// See https://aka.ms/new-console-template for more information

using AdventOfCode2021.Parser;
using System;

public class Program
{

    public static void Main(string[] args)
    {
        Console.WriteLine("Processing...");

        var epicParser = new EpicParser(args[0], "", true);
        var input = epicParser.ToTwoDimensionalList(Environment.NewLine);
        var algo = new Algo(input);
        Console.WriteLine(algo.Result);
    }

}

