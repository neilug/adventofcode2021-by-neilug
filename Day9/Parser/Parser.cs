using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace AdventOfCode2021.Parser
{
    public class EpicParser
    {
        private string _validCharacters;
        private string _fileName;
        private string _fileContent;
        private bool _enableLogs;

        public bool IsValid => IsFileFound && !IsFileEmptyOrWhiteSpace && IsFileContentValid;
        public readonly bool IsFileFound;
        public readonly bool IsFileEmptyOrWhiteSpace;
        public readonly bool IsFileContentValid;
        public string ErrorLog { get; private set; }

        public EpicParser(string fileName, string validCharacters = "", bool enableLogs = false)
        {
            _fileName = fileName;
            _validCharacters = validCharacters;
            _enableLogs = enableLogs;
            
            _fileContent = string.Empty;
            ErrorLog = string.Empty;

            IsFileFound = VerifyFileExist();
            IsFileEmptyOrWhiteSpace = VerifyFileEmptyOrWhiteSpace();
            IsFileContentValid = VerifyFileContent();
        }

        public List<int> ToList(string separator)
        {
            if (!IsValid)
            {
                DisplayErrorMessage(new InvalidDataException().Message);
                DisplayErrorMessage(_fileContent);
                return new List<int>();
            }
            return _fileContent
                .Split(separator)
                .Select(int.Parse)
                .ToList();
        }

        // private int GetXAxis(string separator)
        // {
        //     int k = 0;

        //     while (_fileContent[k] != separator || k < _fileContent.Length)
        //     {
        //         ++k;
        //     }
        //     return k;
        // }

        // private int GetYAxis(string separator)
        // {
        //     var result = 0;

        //     foreach (var line in _fileContent.Split(separator))
        //     {
        //         ++result;
        //     }
        //     return result;
        // }

        public List<List<int>> ToTwoDimensionalList(string separator)
        {
            var result = new List<List<int>>();

            if (!IsValid)
            {
                DisplayErrorMessage(new InvalidDataException().Message);
                DisplayErrorMessage(_fileContent);
                return result;
            }

            foreach(var line in _fileContent.Split(separator).ToList())
            {
                var newLineList = new List<int>();

                foreach (var c in line)
                {
                    newLineList.Add(int.Parse(c.ToString()));
                }
                result.Add(newLineList);
            }

            return result;
        }

        private bool VerifyFileExist()
        {
            try
            {
                _fileContent = File.ReadAllText(_fileName);
                return true;
            }
            catch (Exception exception)
            {
                ErrorLog = exception.Message;
                DisplayErrorMessage(exception.Message);
            }
            return false;
        }


        private bool VerifyFileEmptyOrWhiteSpace()
        {
            return string.IsNullOrWhiteSpace(_fileContent);
        }

        private bool VerifyFileContent()
        {
            if (string.IsNullOrEmpty(_validCharacters))
                return true;
                
            foreach(var c in _fileContent)
            {
                if (!_validCharacters.Contains(c))
                    return false;
            }

            return true;
        }

        private void DisplayErrorMessage(string errorMessage)
        {
            if (_enableLogs)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(" $> ");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(errorMessage);
                Console.ResetColor();
            }
        }
    }
}
